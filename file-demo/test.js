const FS = require('fs');
const PATH = require('path');

const TOOLS = require('./util.js');

const UTF8 = 'utf8';

const appPagePath = './home/home.page.ts';
const appPageText = FS.readFileSync(appPagePath, UTF8);
// console.log(appPageText);
const appPageInnerText = TOOLS.iGetInnerText(appPageText);
console.log(appPageInnerText);

const wxPageJson = {
    data: {
        name: 'ty'
    },
    onLoad() {}
};

// const wxPageText = `Page();`;

// console.log(wxPageText);

// 对象遍历
Object.keys(wxPageJson).forEach(key => {
    console.log(key, wxPageJson[key], typeof wxPageJson[key]);
});
