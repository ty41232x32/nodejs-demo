const fs = require("fs");

const jsdom = require("jsdom");
const { JSDOM } = jsdom;


const data = fs.readFileSync('./home/home.page.html', 'utf8');
// console.log(data);
const dom = new JSDOM(data);
// dom.window.document.querySelector("ion-content").innerHTML;

const elements = dom.window.document.getElementsByTagName('*');
for (let index = 0; index < elements.length; index++) {
    const element = elements[index];
    // let eleHtml = element.outerHTML;

    // TODO 点击事件 传参未处理
    let clickValue = element.getAttribute("(click)");
    if (clickValue) {
        element.removeAttribute("(click)");
        element.setAttribute("bindtap", clickValue.substr(0, clickValue.indexOf("(")));
    }

    let ifValue = element.getAttribute("*ngIf");
    if (ifValue) {
        element.removeAttribute("*ngIf");
        element.setAttribute("wx:if", `{{${ifValue}}}`);
    }

    let forValue = element.getAttribute("*ngFor");
    if (forValue) {
        let forValueArrays = forValue.split(" ");
        element.removeAttribute("*ngFor");
        element.setAttribute("wx:for", `{{${forValueArrays[3]}}}`);
        element.setAttribute("wx:for-index", `i`);
        element.setAttribute("wx:for-item", `${forValueArrays[1]}`);
    }

    let ngModel = element.getAttribute("[(ngModel)]");
    if(ngModel){
        element.removeAttribute("[(ngModel)]");
        console.log(element.outerHTML);
    }
}