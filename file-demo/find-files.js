const FS = require('fs');

var PATH = require('path');

/**
 * 递归遍历文件
 * @param  filePath
 */
function fileDisplay(filePath) {
    FS.readdir(filePath, (err, files) => {
        if (err) {
            console.warn(err);
            return;
        }
        files.forEach(function(filename) {
            var filedir = PATH.join(filePath, filename);
            FS.stat(filedir, function(eror, stats) {
                if (eror) {
                    console.warn('获取文件stats失败');
                } else {
                    var isFile = stats.isFile();
                    var isDir = stats.isDirectory();
                    if (isFile) {
                        console.log(filedir);
                    }
                    if (isDir) {
                        fileDisplay(filedir);
                    }
                }
            });
        });
    });
}

// let filePath = PATH.resolve('D:/project/2017/qingyan/front/xa-ionic4/src/app/pages');
// fileDisplay(filePath);

var copy = function(src, dst) {
    let paths = FS.readdirSync(src); //同步读取当前目录
    paths.forEach(function(path) {
        var _src = src + '/' + path;
        var _dst = dst + '/' + path;
        FS.stat(_src, function(err, stats) {
            //stats  该对象 包含文件属性
            if (err) throw err;
            if (stats.isFile()) {
                //如果是个文件则拷贝
                let readable = FS.createReadStream(_src); //创建读取流
                let writable = FS.createWriteStream(_dst); //创建写入流
                readable.pipe(writable);
            } else if (stats.isDirectory()) {
                //是目录则 递归
                checkDirectory(_src, _dst, copy);
            }
        });
    });
};

var checkDirectory = function(src, dst, callback) {
    FS.access(dst, FS.constants.F_OK, err => {
        if (err) {
            FS.mkdirSync(dst);
            callback(src, dst);
        } else {
            callback(src, dst);
        }
    });
};

const SOURCES_DIRECTORY = 'D:/project/2017/qingyan/front/xa-ionic4/src/app/pages/'; //源目录
checkDirectory(SOURCES_DIRECTORY, './target/pages/', copy);

