const FS = require('fs');
const PATH = require('path');

const UTF8 = 'utf8';

const xtReadFile = (path, callback) => {
    /* FS.readFile(PATH.join(__dirname, path), UTF8, (err, data) => {
        if (err) {
            throw err;
        }
        callback(data);
    }); */
    const data = FS.readFileSync(path, UTF8);
    callback(data);
};

const xtWriteFile = (path, data) => {
    /* FS.writeFile(path, data, UTF8, err => {
        if (err) {
            throw err;
        }
    }); */
    FS.writeFileSync(path, data, UTF8);
};

/**
 * 递归创建目录
 */
const makeDirs = path => {
    if (FS.existsSync(path)) {
        return true;
    }
    if (makeDirs(PATH.dirname(path))) {
        FS.mkdirSync(path);
        return true;
    }
};

//读取文件，并替换内容，生成新的文件
const oldFilePath = './json/web-view.json';
const newFileName = 'web-view.json';
const newFilePath = './wx/pages/';
const newFileFullPath = newFilePath + newFileName;

const data = FS.readFileSync(oldFilePath, UTF8);
const jsonData = JSON.parse(data);
console.log(data);
jsonData.navigationBarTitleText = '个人中心';
console.log(jsonData);
const newFileContent = JSON.stringify(jsonData);
makeDirs(newFilePath);
FS.writeFileSync(newFileFullPath, newFileContent, UTF8);
