/**
 * 去除字符串中的空格、回车换行
 * @param {*} testStr
 */
const iGetInnerText = testStr => {
    var resultStr = testStr.replace(/\s/g, '');
    resultStr = resultStr.replace(/[\r\n]/g, '');
    return resultStr;
};

module.exports = {
    iGetInnerText
};
